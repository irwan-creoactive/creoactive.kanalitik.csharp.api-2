﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{
    [TestClass]
    public class FeedbackClientModelTest
    {
        FeedbackClientModel model = new FeedbackClientModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new FeedbackClient
            {
                customer_login = "cr30active@gmail.com",
                username = "creoactive",
                category = "category",
                message = "(Create) message",
                attachment = "",
                status = "Open",
                modify_status = "1",
                feedbacks = new List<Feedback>() { 
                    new Feedback(){
                    user_id = "1",
                    username = "username",
                    comment = "comment",
                    attachment = "",
                    created_by = model.IdGuid, // id user login
                    modified_by = model.IdGuid, // id user login
                    created_date = DateTime.Now.ToString(),
                    modified_date = DateTime.Now.ToString()
                    }
                },
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.Now,
                modified_date = DateTime.Now
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResult = "cr30active@gmail.com";
            Assert.AreEqual(expectedResult, actual.customer_login);
        }

        [TestMethod]
        public void updateOneTest()
        {
            var actual = model.updateOne(new FeedbackClient
             {
                 id = new ObjectId("554a041c43faee144c021863"),
                 customer_login = "cr30active@gmail.com",
                 username = "creoactive",
                 category = "category",
                 message = "message",
                 attachment = "",
                 status = "Close",
                 modify_status = "1",
                 modified_by = model.IdGuid,
                 modified_date = DateTime.Now
             }).GetAwaiter().GetResult();

            Assert.AreEqual("cr30active@gmail.com", actual.customer_login);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("554a041c43faee144c021863").GetAwaiter().GetResult();
            Assert.AreEqual("554a041c43faee144c021863", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("554a05d843faee1710b695e1").GetAwaiter().GetResult();
            Assert.AreEqual("554a05d843faee1710b695e1", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("customer_login", "cr30active@gmail.com")), new BsonDocument(new BsonElement("category", -1)), 0, 1).GetAwaiter().GetResult();
            Assert.AreEqual(1, actual.Length);

        }

    }
}
