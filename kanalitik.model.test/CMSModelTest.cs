﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;

namespace kanalitik.model.test
{
    [TestClass]
    public class CMSModelTest
    {
        CMSModel model = new CMSModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new Cms
            {
                category = "cms",
                component = "main_video_1_play",
                content = "(Create) Watch Video",
                content_in = "Lihat Video",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.Now,
                modified_date = DateTime.Now
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResult = "cms";
            Assert.AreEqual(expectedResult, actual.category);
        }

        [TestMethod]
        public void updateOneTest()
        {
            var actual = model.updateOne(new Cms
             {
                 id = new ObjectId("5549dccd43faee1618ef0f31"),
                 category = "cms",
                 component = "main_video_1_play",
                 content = "Watch Video",
                 content_in = "Lihat Video",
                 modified_by = model.IdGuid,
                 modified_date = DateTime.Now
             }).GetAwaiter().GetResult();

            Assert.AreEqual("cms", actual.category);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("5549dccd43faee1618ef0f31").GetAwaiter().GetResult();
            Assert.AreEqual("5549dccd43faee1618ef0f31", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("5549dd2443faef140caa0257").GetAwaiter().GetResult();
            Assert.AreEqual("5549dd2443faef140caa0257", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("category", "cms")), new BsonDocument(new BsonElement("category", -1)), 0, 1).GetAwaiter().GetResult();
            Assert.AreEqual(1, actual.Length);

        }

    }
}
