﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class RequestDemoModelTest
    {
        RequestDemoModel model = new RequestDemoModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new RequestDemo
            {
                user_id = "554a01039c876e1fbc32a3ce",
                approved = "false",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResultName = "false";
            Assert.AreEqual(expectedResultName, actual.approved);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new RequestDemo
            {
                id = new ObjectId("554a1bb19c876e15b4633fcd"),
                user_id = "554a01039c876e1fbc32a3ce",
                approved = "true",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("true", actual.approved);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            //var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            //var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
