﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class StreamMenuModelTest
    {
        StreamMenuModel model = new StreamMenuModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new StreamMenu
            {
                client_id = model.IdGuid,
                icon_cls = "fa_book",
                streams = new List<streams>() { },
                title_menu = "Streams menu",


                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow,
            };

            var actual = model.createOne(data).GetAwaiter().GetResult();
            //Assert.AreEqual("Creoactive", actual.name);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new StreamMenu
            {
                id = new ObjectId("554a00ccd2b8de1940cee7a4"),

                streams = new List<streams>() { 
                new streams(){
                account_id = model.IdGuid,
                is_close = true,
                keyword_by_user="waw keren",
                refresh_time_in_second = 5,
                rss_url_by_user="",
                sequence =1,
                stream_title="facebook profile",
                streams_id=model.IdGuid
                }
                },


                modified_by = model.IdGuid, // id user login
                modified_date = DateTime.UtcNow,
            }).GetAwaiter().GetResult();

            //Assert.AreEqual("Creoactive Update", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual.id.ToString());
        }

        //[TestMethod]
        //public void isEmailExistTest()
        //{
        //    var actual = model.isExist("mail@mail.com").GetAwaiter().GetResult();
        //    Assert.AreEqual("mail@mail.com", actual.email);
        //}

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "tru3.d3v@gmail.com")), new BsonDocument(new BsonElement("name", -1)), 0, 1).GetAwaiter().GetResult();

            Assert.AreEqual(1, actual.Length);

        }

    }
}
