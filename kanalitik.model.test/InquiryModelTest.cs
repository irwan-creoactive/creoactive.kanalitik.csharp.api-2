﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;

namespace kanalitik.model.test
{
    [TestClass]
    public class InquiryModelTest
    {
        InquiryModel model = new InquiryModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new Inquiry
            {
                email = "puputangelina@gmail.com",
                name = "putri",
                phone_number = "(Create) 8234758378",
                message = "error",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.Now,
                modified_date = DateTime.Now
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResult = "puputangelina@gmail.com";
            Assert.AreEqual(expectedResult, actual.email);
        }

        [TestMethod]
        public void updateOneTest()
        {
            var actual = model.updateOne(new Inquiry
             {
                 id = new ObjectId("5549e36743faf1140c918345"),
                 email = "puputangelina@gmail.com",
                 name = "putri",
                 phone_number = "8234758378",
                 message = "error",
                 modified_by = model.IdGuid,
                 modified_date = DateTime.Now
             }).GetAwaiter().GetResult();

            Assert.AreEqual("puputangelina@gmail.com", actual.email);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("5549e36743faf1140c918345").GetAwaiter().GetResult();
            Assert.AreEqual("5549e36743faf1140c918345", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("5549e59143faf5140c5237b4").GetAwaiter().GetResult();
            Assert.AreEqual("5549e59143faf5140c5237b4", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "puputangelina@gmail.com")), new BsonDocument(new BsonElement("email", -1)), 0, 1).GetAwaiter().GetResult();
            Assert.AreEqual(1, actual.Length);

        }

    }
}
