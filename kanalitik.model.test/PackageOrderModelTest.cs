﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class PackageOrderModelTest
    {
        PackageOrderModel model = new PackageOrderModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new PackageOrder
            {
                client_id = "5549d41ed2b8de0f20376c41",
                expired_at = DateTime.UtcNow.AddDays(30),
                invoice = "0001",
                order_status = OrderStatusEnum.Open.ToString(),
                package_id = "5549d9e9d2b8de03880a7093",
                status = PackageStatusEnum.Unpaid.ToString(),
                user_id = "5549dd910234c929a86bdb53",

                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow,
            };

            var actual = model.createOne(data).GetAwaiter().GetResult();
            //Assert.AreEqual("Creoactive", actual.name);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new PackageOrder
            {
                id = new ObjectId("5549f328d2b8de1ab0dfb50c"),
                order_status = OrderStatusEnum.Close.ToString(),
                status = PackageStatusEnum.Paid.ToString(),


                modified_by = model.IdGuid, // id user login
                modified_date = DateTime.UtcNow,
            }).GetAwaiter().GetResult();

            //Assert.AreEqual("Creoactive Update", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual.id.ToString());
        }

        //[TestMethod]
        //public void isEmailExistTest()
        //{
        //    var actual = model.isExist("mail@mail.com").GetAwaiter().GetResult();
        //    Assert.AreEqual("mail@mail.com", actual.email);
        //}

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "tru3.d3v@gmail.com")), new BsonDocument(new BsonElement("name", -1)), 0, 1).GetAwaiter().GetResult();

            Assert.AreEqual(1, actual.Length);

        }

    }
}
