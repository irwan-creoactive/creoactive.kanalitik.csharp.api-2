﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;

namespace kanalitik.model.test
{
    [TestClass]
    public class SMSInboxModelTest
    {
        SMSInboxModel model = new SMSInboxModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new SmsInbox
            {
                from = "+6285695130778",
                message = "Nasabah BRI Yth Norekening anda\nMendapakan Cek tunai 27jt\nDari Untung Beliung Britama\nPIN (025998875) U/info hadiah klik www.gebyar-untungbeliung.blogspot.com",
                date = DateTime.Now.ToString(),
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.Now,
                modified_date = DateTime.Now
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResult = "+6285695130778";
            Assert.AreEqual(expectedResult, actual.from);
        }

        [TestMethod]
        public void updateOneTest()
        {
            var actual = model.updateOne(new SmsInbox
             {
                 id = new ObjectId("5549c21743faee1bc44413c2"),
                 from = "+6285695130778",
                 message = "(Update) Nasabah BRI Yth Norekening anda\nMendapakan Cek tunai 27jt\nDari Untung Beliung Britama\nPIN (025998875) U/info hadiah klik www.gebyar-untungbeliung.blogspot.com",
                 date = DateTime.Now.ToString(),
                 modified_by = model.IdGuid,
                 modified_date = DateTime.Now
             }).GetAwaiter().GetResult();

            Assert.AreEqual("+6285695130778", actual.from);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("5549c21743faee1bc44413c2").GetAwaiter().GetResult();
            Assert.AreEqual("5549c21743faee1bc44413c2", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("5549c21743faee1bc44413c2").GetAwaiter().GetResult();
            Assert.AreEqual("5549c21743faee1bc44413c2", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("from", "+6285695130778")), new BsonDocument(new BsonElement("from", -1)), 0, 1).GetAwaiter().GetResult();
            Assert.AreEqual(1, actual.Length);

        }

    }
}
