﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class ConfigMessageContentTest
    {
        ConfigMessageContentModel model = new ConfigMessageContentModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new ConfigMessageContent
            {
                message_type = "mail",
                code = "reset_message",
                message = "You're password already reset",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResultName = "mail";
            Assert.AreEqual(expectedResultName, actual.message_type);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new ConfigMessageContent
            {
                id = new ObjectId("554a0ff89c876e00b8c30226"),
                message_type = "message",
                code = "reset_message",
                message = "You're password already reset",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("message", actual.message_type);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            //var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            //var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
