﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class LogPop3EmailModelTest
    {
        LogPop3EmailModel model = new LogPop3EmailModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new LogPop3Email
            {
                from = "puputangelina@gmail.com",
                subject = "Kanalitik - User Activation",
                body = "Isi Body",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            var expectedResultName = "puputangelina@gmail.com";
            Assert.AreEqual(expectedResultName, actual.from);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new LogPop3Email
            {
                id = new ObjectId("5549f9589c876e18b8d9d142"),
                from = "fahmi@gmail.com",
                subject = "Kanalitik - User Activation",
                body = "Isi Body",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("fahmi@gmail.com", actual.from);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            //var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            //var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            //Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
