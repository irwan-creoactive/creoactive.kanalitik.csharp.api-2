﻿using kanalitik.appconfig;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public partial class AdministrationRepository : BaseRepository, IAdministrationService
    {
        KanalitikAppConfig cfg;
        public AdministrationRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
        }

        public AdministrationRepository()
        {

        }

        public ActorResponse<OrderResponse> packageOrder(ActorRequest<OrderRequest> form)
        {
            ActorResponse<RegisterResponse> response = new ActorResponse<RegisterResponse>();
            response.Result = new RegisterResponse { };

            try
            {
                #region check package
                #endregion

                #region insert document
                #endregion

                #region generate invoice
                #endregion

                #region Attribute Success
                response.Result.message = "OK";
                response.Result.status = true;
                response.Result.success = true;
                #endregion
            }
            catch (Exception e)
            {
                response.Result.message = e.Message;
            }

            throw new NotImplementedException();
        }

        public ActorResponse<BaseResponse> checkPackageIsExist(ActorRequest<string> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<List<PackageListResponse>> showPackageList()
        {
            throw new NotImplementedException();
        }

        public ActorResponse<List<PackageListResponse>> showPackageDetailList(ActorRequest<string> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<OrderPackageResponse> orderPackage(ActorRequest<OrderPackageRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<PackageActivationResponse> packageActivation(ActorRequest<PackageActivationRequest> form)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<PackageResponse> packageSave(ActorRequest<PackageRequest> form)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<PackageResponse> packageList(ActorRequest<PackageRequest> form)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<bool> isPackageClient(ActorRequest<PackageCompareRequest> package_compare)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<OrderPackageResponse> renewPackage(ActorRequest<OrderPackageRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<bool> approveRenewPackage(ActorRequest<string> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<OrderPackageResponse> downgradePackage(ActorRequest<OrderPackageRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<OrderPackageResponse> upgradePackage(ActorRequest<OrderPackageRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<bool> approveUpDownGradePackage(ActorRequest<string> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<bool> isUserOwner(ActorRequest<UserOwnerRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<bool> stopSubscribe(ActorRequest<StopSubscribeRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<ClientSettingResponse> clientSetting(ActorRequest<ClientSettingRequestApi> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountSettingResponse> accountSetting(ActorRequest<AccountSettingRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<InviteClientResponse> inviteMemberClient(ActorRequest<InviteClientRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<InviteClientResponse> editMemberClient(ActorRequest<MemberClientRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<InviteClientResponse> deleteMemberClient(ActorRequest<MemberClientRequest> request)
        {
            throw new NotImplementedException();
        }
    }
}
