﻿using kanalitik.appconfig;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kanalitik.models;
using kanalitik.data.access.documents;

namespace kanalitik.repositories
{
    public partial class MasterDataRepository : BaseRepository, IMasterDataService
    {
        KanalitikAppConfig cfg;
        protected PackageModel packageModel;
        public MasterDataRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
            this.packageModel = new PackageModel();
        }

        public MasterDataRepository()
        {

        }

        public kanalitik.repositories.extends.ReaderConfigFromDB ConfiDb
        {
            get
            {
                return new extends.ReaderConfigFromDB();
            }
        }


        public ActorResponse<UserResponse> saveUser(ActorRequest<UserRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<UserResponse> readUser(ActorRequest<UserRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<UserPackagesResponse> saveUserPackages(ActorRequest<UserPackagesRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<UserPackagesResponse> readUserPackages(ActorRequest<UserPackagesRequest> request)
        {
            throw new NotImplementedException();
        }

        #region Package
        public ActorResponse<MasterDataPackageResponse> saveMasterDataPackage(ActorRequest<MasterDataPackageRequest> request)
        {
            ActorResponse<MasterDataPackageResponse> response = new ActorResponse<MasterDataPackageResponse>();
            response.Result = new MasterDataPackageResponse { };

            try
            {

                var validation = request.validateModel(request.data, request.data.CultureName, "PackageCreateRuleSet");

                #region Validation Block
                if (!validation.IsValid)
                {
                    request.throwErrorSystem(validation);
                }
                #endregion

                #region insert document
                this.packageModel.setCulture(request.data.CultureName);
                this.packageModel.createOne(new Package
                {
                    blog = request.data.blog,
                    is_show = request.data.is_show,
                    module = new List<ModulePackage>() { },
                    news_portal = new string[] { },
                    price = request.data.price,
                    social_media = request.data.social_media,
                    created_by = request.data.UserId,
                    modified_by = request.data.UserId,
                    created_date = DateTime.UtcNow,
                    modified_date = DateTime.UtcNow,

                }).GetAwaiter().GetResult();
                #endregion

                #region Attribute Success
                response.Result.message = "OK";
                response.Result.status = true;
                response.Result.success = true;
                #endregion

            }
            catch (Exception e)
            {
                response.Result.message = e.Message;
            }

            return response;
        }

        public ActorResponse<MasterDataPackageResponse> readMasterDataPackage(ActorRequest<MasterDataPackageRequest> request)
        {
            throw new NotImplementedException();
        }
        #endregion

        public ActorResponse<MasterDataPackageDetailResponse> saveMasterDataDetailPackage(ActorRequest<MasterDataPackageDetailRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<MasterDataPackageDetailResponse> readMasterDataDetailPackage(ActorRequest<MasterDataPackageDetailRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<InvoicesResponse> saveInvoices(ActorRequest<InvoicesRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<InvoicesResponse> readInvoices(ActorRequest<InvoicesRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<RoleMenuResponse> saveInvoices(ActorRequest<RoleMenuRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<RoleMenuResponse> readInvoices(ActorRequest<RoleMenuRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<MasterDataMenuPackageResponse> saveInvoices(ActorRequest<MasterDataMenuPackageRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<MasterDataMenuPackageResponse> readInvoices(ActorRequest<MasterDataMenuPackageRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogResponse> saveInvoices(ActorRequest<BlogRequestApi> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogResponse> readInvoices(ActorRequest<BlogRequestApi> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogCommentResponse> saveInvoices(ActorRequest<BlogCommentRequestApi> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogCommentResponse> readInvoices(ActorRequest<BlogCommentRequestApi> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<FeedbackResponse> saveInvoices(ActorRequest<FeedbackRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<FeedbackResponse> readInvoices(ActorRequest<FeedbackRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<RequestDemoResponse> saveInvoices(ActorRequest<RequestDemoRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<RequestDemoResponse> readInvoices(ActorRequest<RequestDemoRequest> request)
        {
            throw new NotImplementedException();
        }
    }
}
