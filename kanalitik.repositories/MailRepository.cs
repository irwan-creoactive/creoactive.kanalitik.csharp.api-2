﻿using kanalitik.appconfig;
using kanalitik.data.access.documents;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.interfaces;
using kanalitik.models;
using kanalitik.request.mail;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public partial class MailRepository : BaseRepository, IMailService
    {
        KanalitikAppConfig cfg;
        protected LogEmailModel logEmailModel;
        protected ConfigMessageContentModel configMessageContentModel;

        public MailRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
            this.logEmailModel = new LogEmailModel();
            this.configMessageContentModel = new ConfigMessageContentModel();
        }

        public MailRepository()
        {

        }

        public ActorResponse<BaseResponse> serviceRun(ActorRequest<BaseRequest> form)
        {
            throw new NotImplementedException();
        }

        private ConfigMessageContent[] getEmailConfig(string code)
        {
            var result = this.configMessageContentModel.list(
                new BsonDocument(new BsonElement[]{
                new BsonElement("code",code),
                new BsonElement("message_type","mail")
                }),
                new BsonDocument(),
                0, 1).GetAwaiter().GetResult();
            return result;
        }


        public ActorResponse<MailResponse> mailList(ActorRequest<MailRequest> form)
        {
            throw new NotImplementedException();
        }


        public ActorResponse<MailResponse> mailSave(ActorRequest<MailSaveRequest> form)
        {
            ActorResponse<MailResponse> response = new ActorResponse<MailResponse>();
            response.Result = new MailResponse { };

            try
            {

                var validation = form.validateModel(form.data, form.data.CultureName, "CreateRuleSet");

                #region Validation Block
                if (!validation.IsValid)
                {
                    form.throwErrorSystem(validation);
                }
                #endregion

                #region content email
                var emailConf = this.getEmailConfig(form.data.configCode);
                #endregion

                #region insert document
                this.logEmailModel.setCulture(form.data.CultureName);

                this.logEmailModel.createOne(new LogEmail
                {
                    attachment = new ObjectId().ToString(),
                    body = emailConf[0].message,
                    date_create = DateTime.UtcNow,
                    date_sent = DateTime.UtcNow,
                    message = "",
                    sender = "ADMIN",
                    status = "Open",
                    subject = emailConf[0].subject,
                    to = form.data.email,

                    created_by = form.data.UserId,
                    modified_by = form.data.UserId,
                    created_date = DateTime.UtcNow,
                    modified_date = DateTime.UtcNow,
                }).GetAwaiter().GetResult();
                #endregion

                #region Attribute Success
                response.Result.message = "OK";
                response.Result.status = true;
                response.Result.success = true;
                #endregion

            }
            catch (Exception e)
            {
                response.Result.message = e.Message;
            }

            return response;
        }
    }
}
