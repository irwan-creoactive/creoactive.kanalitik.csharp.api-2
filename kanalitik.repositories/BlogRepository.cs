﻿using kanalitik.appconfig;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public partial class BlogRepository : BaseRepository, IBlogService
    {
        KanalitikAppConfig cfg;
        public BlogRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
        }

        public BlogRepository()
        {


        }

        public ActorResponse<BlogResponse> createBlog(ActorRequest<request.blog.BlogRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogResponse> updateBlog(ActorRequest<request.blog.BlogRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogResponse> publishBlog(ActorRequest<request.blog.BlogRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogResponse> unpublishBlog(ActorRequest<request.blog.BlogRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<ListBlogResponse> viewListPublishBlog()
        {
            throw new NotImplementedException();
        }

        public ActorResponse<ListBlogResponse> viewAllListBlog()
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogResponse> viewDetailBlog(ActorRequest<request.blog.BlogRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogCommentResponse> insertBlogComment(ActorRequest<request.blog.BlogCommentRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BlogCommentResponse> approveBlogComment(ActorRequest<request.blog.BlogCommentRequest> request)
        {
            throw new NotImplementedException();
        }
    }
}
