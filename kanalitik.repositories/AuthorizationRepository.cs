﻿using kanalitik.appconfig;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public partial class AuthorizationRepository:BaseRepository,IAuthorizationService
    {
        KanalitikAppConfig cfg;
        public AuthorizationRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
        }

        public AuthorizationRepository()
        {

        }
    }
}
