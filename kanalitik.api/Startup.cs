﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using Microsoft.Owin;
using Owin;
using Akka.Actor;
using Akka.Configuration.Hocon;
using kanalitik.akka.actors;
using kanalitik.akka.actors.core;

[assembly: OwinStartup(typeof(kanalitik.api.Startup))]

namespace kanalitik.api
{
    public partial class Startup
    {
     
        public void Configuration(IAppBuilder app)
        {
            //Config Akka
            BootstrapActor.Initialize();

            ConfigureAuth(app);
        }
    }
}
