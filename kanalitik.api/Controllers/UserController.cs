﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Configuration;
using Akka;
using Akka.Actor;
using Akka.Configuration.Hocon;
using kanalitik.akka.actors;
using kanalitik.data.contract;
using kanalitik.akka.actors.core;
using kanalitik.constanta;
using kanalitik.data.contract.apiResponse;
using kanalitik.data.contract.apiRequest;
using kanalitik.akka.actors.user;
using System.Web.Http.Cors;


namespace kanalitik.api.Controllers
{
    [EnableCors(origins: "syndicate73.ddns.net:44304", headers: "*", methods: "GET, POST, OPTIONS")]
    public class UserController : ApplicationController
    {
        public UserController()
        {
            
        }

        [HttpPost]
        public async Task<ActorResponse<RegisterResponse>> register(RegisterRequestApi request)
        {
            #region //Contoh Direct
            //var greeter = System.ActorSelection(String.Format(Const.actorUserPath, "register"));
            //var result = await greeter.Ask<Object>(new ActorRequest {  Interface = "hai" }, Const.DefaultTimeout);
            //return Ok(new { msg = result });
            #endregion

            var actorIndex = await GetActor<SignUpActor>();
            return await actorIndex.Ask<ActorResponse<RegisterResponse>>(request, Const.DefaultTimeout);
        }
    }
}
