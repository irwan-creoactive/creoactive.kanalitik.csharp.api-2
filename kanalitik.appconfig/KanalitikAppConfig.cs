﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.appconfig
{
    public class KanalitikAppConfig
    {
        public string FaceBookClientId { set; get; }
        public KanalitikAppConfig()
        {
            this.FaceBookClientId = ConfigurationManager.AppSettings["fb_client_id"];
        }
    }
}
