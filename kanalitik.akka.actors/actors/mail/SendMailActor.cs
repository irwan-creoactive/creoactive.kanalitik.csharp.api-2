﻿using Akka.Actor;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.mail
{
    public class SendMailActor : UntypedActor
    {
        IMailService svc;
        public SendMailActor(IMailService _svc)
        {
            svc = _svc;
        }

        /// <summary>
        /// ini buat menjalankan service mail
        /// </summary>
        /// <param name="message"></param>
        protected override void OnReceive(object message)
        {

            System.IO.File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + @"log.txt", new string[] { DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() });

        }
    }
}
