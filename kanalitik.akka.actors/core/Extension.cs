﻿using Akka.Actor;
using kanalitik.akka.actors.mail;
using kanalitik.akka.actors.user;
using kanalitik.appconfig;
using kanalitik.constanta;
using kanalitik.interfaces;
using kanalitik.repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.core
{
    public class Extension : IExtension
    {
        public Extension(ExtendedActorSystem system)
        {

            // Load Config
            KanalitikAppConfig myAppConfig = new KanalitikAppConfig();

            //Ref Services
            IMailService svcMailSave = new MailRepository(myAppConfig);
            IUserService svcReguser = new UserRepository(myAppConfig);
            IAdministrationService svcAdmin = new AdministrationRepository(myAppConfig);
            IAuthenticationService svcAuth = new AuthenticationRepository(myAppConfig);
            IAuthorizationService svcAuthor = new AuthorizationRepository(myAppConfig);
            IBlogService svcBlog = new BlogRepository(myAppConfig);
            IContentManagementService svcCMS = new ContentManagemenRepository(myAppConfig);
            ICrawlingService svcCrawling = new CrawlingRepository(myAppConfig);
            IMailService svcMailService = new MailRepository(myAppConfig);
            IMasterDataService svcMasterData = new MasterDataRepository(myAppConfig);
            IReportService svcReport = new ReportRepository(myAppConfig);
            IRssService svcRss = new RssRepository(myAppConfig);
            ISocialMediaService svcSocialMedia = new SocialMediaRepository(myAppConfig);


            // Create Actors
            var signUpActor = system.ActorOf(Props.Create(() => new SignUpActor(svcReguser)), "signup");
            var mailActor = system.ActorOf(Props.Create(() => new SendMailActor(svcMailService)), "sendemail");
            var mailSaveActor = system.ActorOf(Props.Create(() => new MailSaveActor(svcMailSave)), "mailsave");


            // scheduler
            var cancellation = new Cancelable(system.Scheduler);
            system.Scheduler
                .ScheduleTellRepeatedly(Const.DelayActorScheduler, // initial delay of  second
                 Const.TickActorScheduler, // recur every  seconds
                 mailActor, "mail_job", ActorRefs.Nobody, cancellation);
            //cancellation.Cancel(); 


            // Passing to addres atau path actor
            var addressBook = system.ActorOf(Props.Create(() => new AddressBook(new Dictionary<Type, ICanTell>
            {
                {typeof(SignUpActor), signUpActor},
                {typeof(MailSaveActor), mailSaveActor},
            })), AddressBook.Name);


        }

    }
    public class ExtensionProvider : ExtensionIdProvider<Extension>
    {
        public static readonly ExtensionProvider Instance = new ExtensionProvider();
        public ExtensionProvider() { }
        public override Extension CreateExtension(ExtendedActorSystem system)
        {
            return new Extension(system);
        }
    }
}
