﻿using Akka.Actor;
using Akka.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.core
{
    public class BootstrapActor
    {
        private static ActorSystem _system;

        public static ActorSystem System
        {
            get
            {
                if (_system == null) throw new InvalidOperationException("Actor system is not initialized");
                return _system;
            }
        }

        public static void Initialize()
        {
            _system = ActorSystem.Create("MyContainerServer", ConfigurationFactory.Load());

            ExtensionProvider.Instance.Apply(_system);
        }

        public static void Shutdown()
        {
            if (_system != null)
            {
                _system.Shutdown();
                _system.AwaitTermination(TimeSpan.FromMinutes(1));

                _system = null;
            }
        }
    }
}
