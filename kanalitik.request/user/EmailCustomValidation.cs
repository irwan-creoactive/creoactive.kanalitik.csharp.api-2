﻿using kanalitik.models;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.request.user
{

    public class EmailCustomValidatorData : ValueValidatorData
    {
        public EmailCustomValidatorData(){}
        public EmailCustomValidatorData(string name)
            : base(name, typeof(EmailCustomValidator)){}
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new EmailCustomValidator();
        }
    }

    [ConfigurationElementType(typeof(EmailCustomValidatorData))]
    public class EmailCustomValidator : Validator<string>
    {
        UserModel usermodel;
        public EmailCustomValidator()
            : base(null, null)
        {
            usermodel = new UserModel();
        }

        protected override string DefaultMessageTemplate
        {
            get
            {
                return "";
            }
        }

        /// <summary>
        /// --------> Logic Custom Validation <-------
        /// Cek apakah email ini sudah ada atau tidak
        /// </summary>
        /// <param name="objectToValidate"></param>
        /// <param name="currentTarget"></param>
        /// <param name="key"></param>
        /// <param name="validationResults"></param>
        protected override void DoValidate(string objectToValidate, object currentTarget, string key, ValidationResults validationResults)
        {
            
            var user = usermodel.isEmailExist(objectToValidate).GetAwaiter().GetResult();

            if (user.email != null) //Cek apakah email sudah ada?
            {
                // Throw message
                var msg = this.MessageTemplate.Replace("{email}",objectToValidate);

                LogValidationResult(validationResults, msg, currentTarget, key);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Parameter, AllowMultiple = true,Inherited = false)]
    public class EmailCustomValidatorAttribute : ValidatorAttribute
    {
        public EmailCustomValidatorAttribute(){}
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new EmailCustomValidator();

        }
    }
}
