﻿using kanalitik.models;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.request.user
{

    public class BusinessTypeNameCustomValidatorData : ValueValidatorData
    {
        public BusinessTypeNameCustomValidatorData(){}
        public BusinessTypeNameCustomValidatorData(string name)
            : base(name, typeof(BusinessTypeNameCustomValidator)) { }
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new BusinessTypeNameCustomValidator();
        }
    }

    [ConfigurationElementType(typeof(BusinessTypeNameCustomValidatorData))]
    public class BusinessTypeNameCustomValidator : Validator<string>
    {
        UserModel usermodel;
        public BusinessTypeNameCustomValidator()
            : base(null, null)
        {
            usermodel = new UserModel();
        }

        protected override string DefaultMessageTemplate
        {
            get
            {
                return "";
            }
        }

        /// <summary>
        /// --------> Logic Custom Validation <-------
        /// </summary>
        /// <param name="objectToValidate"></param>
        /// <param name="currentTarget"></param>
        /// <param name="key"></param>
        /// <param name="validationResults"></param>
        protected override void DoValidate(string objectToValidate, object currentTarget, string key, ValidationResults validationResults)
        {
            var current = (RegisterRequest)currentTarget;
            if (current.business_type != null)
            {
                // Case jika bukan personal maka harus isi business type name
                if (current.business_type != "Personal" && string.IsNullOrWhiteSpace(objectToValidate))
                {
                    // Throw message
                    LogValidationResult(validationResults, this.MessageTemplate, currentTarget, key);
                }
            }
           
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Parameter, AllowMultiple = true,Inherited = false)]
    public class BusinessTypeNameCustomValidatorAttribute : ValidatorAttribute
    {
        public BusinessTypeNameCustomValidatorAttribute() { }
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new BusinessTypeNameCustomValidator();

        }
    }
}
