﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class PostSchedule : Base
    {
        /**
       * description : menampung data aktifitas post schedule yang dilakukan oleh user
       */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection post_schedule
        /// </summary>

        public string client_id { set; get; }
        /// <summary>
        /// id client; object id collection client
        /// </summary>

        public string schedule_type { set; get; }
        /// <summary>
        /// nama jenis schedule; 
        /// saat ini terbagi menjadi daily, weekly, date; 
        /// daily - jika dijadwalkan untuk posting harian (setiap hari pada jam tertentu); 
        /// weekly - jika dijadwalkan untuk posting mingguan (pada hari dan jam tertentu); 
        /// date - jika dijadwalkan untuk posting pada tanggal tertentu (dengan jam yang ditentukan);
        /// </summary>

        public string date { set; get; }
        /// <summary>
        /// tanggal posting; terisi jika schedule_type diisi date
        /// </summary>

        public string day { set; get; }
        /// <summary>
        /// hari posting; terisi jika schedule_type diisi weekly;
        /// </summary>

        public string time { set; get; }
        /// <summary>
        /// jam posting / waktu posting; 
        /// harus selalu terisi - apapun isian pada atribut schedule_type
        /// </summary>

        public string message { set; get; }
        /// <summary>
        /// isi pesan atau text yang akan diposting;
        /// </summary>

        public string file { set; get; }
        /// <summary>
        /// file id - jika postingan terdapat attachment file;
        /// </summary>

        public List<AccountSocialMediaPostSchedule> account_social_media { set; get; }
        /// <summary>
        /// berisi array object social_media_id; 
        /// memberikan informasi account social media mana saja yang akan melakaukan post schedule ini;
        /// </summary>

        public List<RevisionPostSchedule> revision { set; get; }
        /// <summary>
        /// berisi array object revisi; 
        /// berisi keterangan informasi interaksi revisi antar user pada satu client id
        /// </summary>

        public string status { set; get; }
        /// <summary>
        /// berisi approved - expired - need approve - revision; 
        /// approved - jika post_schedule ini sudah siap posting; 
        /// expired - ketika post_schedule ini sudah melewati batas tanggal dan waktu posting; 
        /// need approve - ketika post_schedule ini tinggal menunggu persetujuan; 
        /// revision - ketika post_schedule ini perlu direvisi isiannya;
        /// </summary>

    }

    public class AccountSocialMediaPostSchedule
    {
        string social_media_id { set; get; }
        /// <summary>
        /// id social media; 
        /// diambil dari object id account social media yang owned account;
        /// </summary>

    }

    public class RevisionPostSchedule
    {
        string sequence { set; get; }
        /// <summary>
        /// nomor urut revisi
        /// </summary>

        string message { set; get; }
        /// <summary>
        /// pesan mengapa perlu direvisi
        /// </summary>

        string user_id { set; get; }
        /// <summary>
        /// user id yang meminta proses revisi;
        /// </summary>

        string status { set; get; }
        /// <summary>
        /// berisi open - close; 
        /// open jika proses revisi masih belum dilakukan; 
        /// close jika proses revisi sudah dilakukan
        /// </summary>

    }
}
