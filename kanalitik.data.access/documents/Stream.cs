﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    /// <summary>
    /// collection ini berisi daftar stream yang terdapat pada aplikasi kanalitik			
    /// </summary>
    public class Stream : Base
    {
        /// <summary>
        /// object id dari collection stream
        /// </summary>
        public ObjectId id { set; get; }
        /// <summary>
        /// berisi nama media; misal Facebook Profile, Twitter Profile, etc
        /// </summary>
        public string media { set; get; }
        /// <summary>
        /// berisi jenis stream; misal Social Media, Blog, atau News Portal
        /// </summary>
        public string group { set; get; }
        /// <summary>
        /// nama stream
        /// </summary>
        public string stream_name { set; get; }
        /// <summary>
        /// alamat url dari web untuk sumber stream
        /// </summary>
        public string url_web { set; get; }
        /// <summary>
        /// alamat url rss untuk sumber data stream
        /// </summary>
        public string url_rss { set; get; }
        /// <summary>
        /// digunakan untuk pengkategorian news portal;
        /// </summary>
        public string categories { set; get; }
        /// <summary>
        /// turunan detil dari categories
        /// </summary>
        public string contents { set; get; }
        /// <summary>
        /// berisi true - false; true jika stream aktif; false jika stream tidak aktif
        /// </summary>
        public bool is_active { set; get; }
        /// <summary>
        /// nomor urut stream; ini berlaku untuk satu media
        /// </summary>
        public int sequence { set; get; }
        /// <summary>
        /// icon yang digunakan untuk stream; berkaitan dengan tampilan;
        /// </summary>
        public string icon_fa_cls { set; get; }
        /// <summary>
        /// berisi array yang menampung aksi-aksi yang dapat dilakukan oleh user terhadap stream ini;
        /// nama aksi yang dapat dilakukan
        /// </summary>
        public string[] actions { set; get; }
    }


}
