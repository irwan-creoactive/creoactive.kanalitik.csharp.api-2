﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class LogEmail : Base
    {
        /**
        * description : isi collection ini adalah antrian untuk mengirimkan email;
        */
        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection log_email
        /// </summary>

        public string sender { set; get; }
        /// <summary>
        /// diambil dari atribut sender_name pada collection config_mail_account
        /// </summary>

        public string to { set; get; }
        /// <summary>
        /// alamat tujuan pengiriman pesan
        /// </summary>

        public string subject { set; get; }
        /// <summary>
        /// judul email
        /// </summary>

        public string body { set; get; }
        /// <summary>
        /// isi email
        /// </summary>

        public string status { set; get; }
        /// <summary>
        /// berisi success - error; 
        /// success jika pengiriman berhasil; 
        /// error jika pengiriman gagal;
        /// </summary>

        public string message { set; get; }
        /// <summary>
        /// pesan kembalian dari status
        /// </summary>

        public DateTime date_sent { set; get; }
        /// <summary>
        /// tanggal pengiriman pesan
        /// </summary>

        public DateTime date_create { set; get; }
        /// <summary>
        /// tanggal pembuatan pesan
        /// </summary>

        public string attachment { set; get; }
        /// <summary>
        /// object id attachment
        /// </summary>

    }
}
