﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace kanalitik.data.access.documents
{
    /// <summary>
    /// collection ini menampung data account social media yang sudah didaftarkan oleh user. Account social media yang didaftarkan pada collection ini ada dua; account social media yang dimiliki oleh user dan account social yang hanya akan di-observe oleh user.			
    /// </summary>
    public class AccountSocialMedia : Base
    {
        /// <summary>
        /// object id collection account_social_media
        /// </summary>
        [BsonId]
        public ObjectId id { set; get; }
        /// <summary>
        /// object id dari collection client
        /// </summary>

        public string client_id { set; get; }
        /// <summary>
        /// id account social media; id ini didapat dari id yang tercatat pada sistem social media
        /// </summary>

        public string social_media_id { set; get; }
        /// <summary>
        ///nama media; ex twitter, facebook etc;
        /// </summary>

        public string media { set; get; }
        /// <summary>
        /// nama account social media; didapat dari sistem social media
        /// </summary>

        public string account_name { set; get; }
        /// <summary>
        /// alamat email account social media; alamat email ini terisi jika memang diakomodir oleh sistem social media yang bersangkutan;
        /// </summary>

        public string email { set; get; }
        /// <summary>
        ///url profile_picture yang digunakan untuk account social media
        /// </summary>

        public string profile_picture { set; get; }
        /// <summary>
        /// token_access untuk mengakses account_social_media
        /// </summary>

        public string token_access { set; get; }
        /// <summary>
        /// token_secret untuk mengakses account_social_media
        /// </summary>

        public string token_secret { set; get; }
        /// <summary>
        /// code untuk melakukan refresh_token; code ini akan berguna ketika akses account_social_media sudah expired
        /// </summary>

        public string refresh_token { set; get; }
        /// <summary>

        /// informasi apakah akun yang didaftarkan aktif atau tidak; berisi ture - false; isian ini didapat dari sistem social media;
        /// </summary>

        public bool is_active { set; get; }
        /// <summary>
        /// atribut ini terisi jika document berisi akun facebook dan merupakan page; berisi App page, brand or product, etc;
        /// </summary>

        public string fb_page_category { set; get; }
        /// <summary>
        /// atribut ini terisi jika document berisi akun facebook dan merupakan page; 
        /// </summary>

        public string fb_page_permission { set; get; }
        /// <summary>
        ///  berisi true - false; true - jika document ini merupakan other account; false - jika document ini merupakan owned account
        /// </summary>

        public bool is_non_authorize { set; get; }
        /// <summary>
        /// durasi expired (dalam detik) - jika tersedia
        /// </summary>

        public int expired_in { set; get; }
        /// <summary>
        /// tanggal expired - jika tersedia
        /// </summary>

        public DateTime expired_date { set; get; }
        /// <summary>
        /// berisi object id dari collection account_social_media; atribut ini diisi khusus untuk account social media yang berupa facebook page atau pun facebook group; dan merujuk pada account facebook admin-nya;
        /// </summary>

        public string parent_social_media_id { set; get; }
        /// <summary>
        /// berisi true - false; true - jika merupakan akun youtube; false - jika bukan merupakan akun youtube;
        /// </summary>

        public bool is_have_youtube_account { set; get; }

    }
}
