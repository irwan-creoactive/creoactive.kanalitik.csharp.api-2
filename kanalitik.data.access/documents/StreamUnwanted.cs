﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class StreamUnwanted : Base
    {
        /**
        * description : berisi daftar post yang tidak diharapkan muncul pada stream; kanalitik memiliki fitur untuk memfilter post yang tidak diharapkan tampil; post tersebut cukup diberitanda dan nanti akan masuk pada collection ini; setiap post yang sudah masuk pada collection ini tidak akan tampil pada stream;
        */

        public ObjectId id { set; get; } 
        /// <summary>
        /// object id collection stream_unwanted
        /// </summary>

        public string social_account_id { set; get; } 
        /// <summary>
        /// object id dari collection account_social_media; 
        /// merupakan account id yang digunakan untuk mencatatkan post yang tidak diinginkan (unwanted)
        /// </summary>

        public string user_id { set; get; } 
        /// <summary>
        /// user yang melakukan aksi unwanted post; 
        /// diambil dari object id collection user;
        /// </summary>

        public string media { set; get; } 
        /// <summary>
        /// nama social media; 
        /// atau blog yang dicatat sebagai unwanted; 
        /// lebih rinci dari social media; 
        /// ex facebook page, facebook group, etc;
        /// </summary>

        public string social_media { set; get; } 
        /// <summary>
        /// nama social media; 
        /// atau blog yang dicatat sebagai unwanted;
        /// </summary>

        public string account_name { set; get; } 
        /// <summary>
        /// nama akun social media terdaftar (owned) yang digunakan untuk malakukan aksi unwanted post;
        /// </summary>

        public string post_id { set; get; } 
        /// <summary>
        /// kode unik dari post; diambil dari api social media;
        /// </summary>

        public string user_post_id { set; get; } 
        /// <summary>
        /// kode unik dari user yang dicatat unwanted; 
        /// diambil dari api social media;
        /// </summary>

        public string blog_id { set; get; } 
        /// <summary>
        /// atribut ini diisi jika media berisi blog; 
        /// diambil dari blog id yang melakukan posting; diambil dari api social media;
        /// </summary>

        public string client_id { set; get; } 
        /// <summary>
        /// client id dari user yang melakukan aksi unwanted post;
        /// </summary>

        public string comment_id { set; get; } 
        /// <summary>
        /// kode unik dari id comment; 
        /// saat ini digunakan untuk unwanted comment pada instagram; 
        /// id diambil dari api social media
        /// </summary>

        public string unwanted_type { set; get; } 
        /// <summary>
        /// berisi post - user - comment; 
        /// post jika yang ditandai adalah postingan; 
        /// user jika yang ditandai adalah id user; 
        /// comment jika yang ditandai adalah comment
        /// </summary>
        
    }
}
