﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Module : Base
    {
        /**
        * description : collection ini menampung daftar menu yang ada pada aplikasi kanalitik. Menu yang disimpan tidak hanya menu sistem, melainkan menu stream yang dibuat oleh user juga dimasukkan pada collection ini.			
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id dari collection module
        /// </summary>

        public string code { set; get; }
        /// <summary>
        /// kode menu 
        /// </summary>

        public string name { set; get; }
        /// <summary>
        /// nama menu 
        /// </summary>
        public string seq { set; get; }
        /// <summary>
        /// urutan kemunculan module
        /// </summary>

        public string description { set; get; }
        /// <summary>
        /// penjelasan singkat tentang menu
        /// </summary>

        public string is_client { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true - jika menu merupakan hasil pembuatan oleh client; 
        /// false - jika menu adalah menu sistem;
        /// </summary>

        public string client_id { set; get; }
        /// <summary>
        /// jika atribut is_client berisi true, maka atribut ini akan berisi id client yang mengisi data module. 
        /// id ini diambil dari object id collection client
        /// </summary>

        public string parent_module_code { set; get; }
        /// <summary>
        /// berisi code dari collection module; 
        /// atribut ini diisi jika module ini merupakan anak dari module yang lain.
        /// </summary>


    }
}
