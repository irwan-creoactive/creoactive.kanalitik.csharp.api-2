﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class PastSchedule : Base
    {
        /**
        * description : collection ini berisi daftar postingan dari post_schedule yang sudah terposting
        */
        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection past_schedule
        /// </summary>

        public string client_id { set; get; }
        /// <summary>
        /// id client yang posting; 
        /// diambil dari object id collection client
        /// </summary>

        public string post_schedule_id { set; get; }
        /// <summary>
        /// id post_schedule; 
        /// merujuk schedule post mana yang diposting; 
        /// diambil dari object id collection post_schedule
        /// </summary>

        public string date { set; get; }
        /// <summary>
        /// tanggal posting
        /// </summary>

        public string time { set; get; }
        /// <summary>
        /// waktu posting
        /// </summary>

        public string message { set; get; }
        /// <summary>
        /// isian pesan posting
        /// </summary>

        public string file { set; get; }
        /// <summary>
        /// file attachment posting (jika tersedia)
        /// </summary>

        public List<AccountSocialMediaSchedule> account_social_media { set; get; }
        /// <summary>
        /// berisi array object social_media_id; 
        /// memberikan informasi account social media mana saja yang akan melakaukan post schedule ini;
        /// </summary>

    }

    public class AccountSocialMediaSchedule
    {
        string social_media_id { set; get; }
        /// <summary>
        /// id social media; 
        /// diambil dari object id account social media yang owned account; 
        /// </summary>

    }
}
