﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Core;
using System.Configuration;
using kanalitik.data.access.documents;

namespace kanalitik.data.access
{
    public class DBKanalitikContext : IDBKanalitikContext
    {
        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;
        static DBKanalitikContext()
        {
            var conStr = ConfigurationManager.AppSettings["MongoDBHost"];
            var conDb = ConfigurationManager.AppSettings["MongoDBName"];
            _client = new MongoClient(conStr);
            _database = _client.GetDatabase(conDb);
        }

        public IMongoClient Client
        {
            get { return _client; }
        }

        public IMongoCollection<User> Users
        {
            get { return _database.GetCollection<User>("user"); }
        }

        public IMongoCollection<AccountSocialMedia> AccountSocialMedia
        {
            get { return _database.GetCollection<AccountSocialMedia>("account_social_media"); }
        }

        public IMongoCollection<Blog> Blog
        {
            get { return _database.GetCollection<Blog>("blog"); }
        }

        public IMongoCollection<BlogSubscribe> BlogSubscribe
        {
            get { return _database.GetCollection<BlogSubscribe>("blog_subscribe"); }
        }

        public IMongoCollection<Client> Clients
        {
            get { return _database.GetCollection<Client>("client"); }
        }

        public IMongoCollection<Cms> Cms
        {
            get { return _database.GetCollection<Cms>("cms"); }
        }

        public IMongoCollection<CodeGenerate> CodeGenerate
        {
            get { return _database.GetCollection<CodeGenerate>("code_generate"); }
        }

        public IMongoCollection<ConfigMailAccount> ConfigMailAccount
        {
            get { return _database.GetCollection<ConfigMailAccount>("config_mail_account"); }
        }

        public IMongoCollection<ConfigMessageContent> ConfigMessageContent
        {
            get { return _database.GetCollection<ConfigMessageContent>("config_message_content"); }
        }

        public IMongoCollection<DiscussionBoard> DiscussionBoard
        {
            get { return _database.GetCollection<DiscussionBoard>("discussion_board"); }
        }

        public IMongoCollection<FeedbackClient> FeedbackClient
        {
            get { return _database.GetCollection<FeedbackClient>("feedback_client"); }
        }

        public IMongoCollection<GeneralConfiguration> GeneralConfiguration
        {
            get { return _database.GetCollection<GeneralConfiguration>("general_configuration"); }
        }

        public IMongoCollection<Inquiry> Inquiry
        {
            get { return _database.GetCollection<Inquiry>("inquiry"); }
        }

        public IMongoCollection<InternetBankMessage> InternetBankMessage
        {
            get { return _database.GetCollection<InternetBankMessage>("internet_bank_message"); }
        }

        public IMongoCollection<Module> Module
        {
            get { return _database.GetCollection<Module>("module"); }
        }

        public IMongoCollection<Feature> Feature
        {
            get { return _database.GetCollection<Feature>("feature"); }
        }

        public IMongoCollection<Keyword> Keyword
        {
            get { return _database.GetCollection<Keyword>("keyword"); }
        }

        public IMongoCollection<LogEmail> LogEmail
        {
            get { return _database.GetCollection<LogEmail>("log_email"); }
        }

        public IMongoCollection<LogPop3Email> LogPop3Email
        {
            get { return _database.GetCollection<LogPop3Email>("log_pop3_email"); }
        }

        public IMongoCollection<Module> Modules
        {
            get { return _database.GetCollection<Module>("module"); }
        }

        public IMongoCollection<Package> Package
        {
            get { return _database.GetCollection<Package>("package"); }
        }

        public IMongoCollection<PackageOrder> PackageOrder
        {
            get { return _database.GetCollection<PackageOrder>("package_order"); }
        }

        public IMongoCollection<PastSchedule> PastSchedule
        {
            get { return _database.GetCollection<PastSchedule>("past_schedule"); }
        }

        public IMongoCollection<PostSchedule> PostSchedule
        {
            get { return _database.GetCollection<PostSchedule>("post_schedule"); }
        }

        public IMongoCollection<RequestDemo> RequestDemo
        {
            get { return _database.GetCollection<RequestDemo>("request_demo"); }
        }

        public IMongoCollection<RoleUser> RoleUser
        {
            get { return _database.GetCollection<RoleUser>("role_user"); }
        }

        public IMongoCollection<SmsInbox> SmsInbox
        {
            get { return _database.GetCollection<SmsInbox>("sms_inbox"); }
        }

        public IMongoCollection<Stream> Stream
        {
            get { return _database.GetCollection<Stream>("stream"); }
        }

        public IMongoCollection<StreamMenu> StreamMenu
        {
            get { return _database.GetCollection<StreamMenu>("stream_menu"); }
        }

        public IMongoCollection<StreamUnwanted> StreamUnwanted
        {
            get { return _database.GetCollection<StreamUnwanted>("stream_unwanted"); }
        }

        public IMongoCollection<UserLogActivity> UserLogActivity
        {
            get { return _database.GetCollection<UserLogActivity>("user_log_activity"); }
        }
    }
}
