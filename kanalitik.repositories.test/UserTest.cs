﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;
using kanalitik.interfaces;

namespace kanalitik.repositories.test
{
    [TestClass]
    public class UserTest
    {

        [TestMethod]
        public void registerTest()
        {
            var container = new UnityContainer();
            container.RegisterType<IUserService, UserRepository>();
            var svc = container.Resolve<IUserService>();

            var req = new kanalitik.data.contract.ActorRequest<kanalitik.request.user.RegisterRequest>();
            req.data = new request.user.RegisterRequest();
            // req.data.CultureName = "id-ID";
            req.data.CultureName = "en-US";

            req.data.name = "Muhamad Muchlis";
            req.data.email = "tru3.d3v@gmail.comro";
            req.data.business_type = "Company"; // Organization // Personal // Company
            req.data.business_type_name = "PT.Sugan waras";
            req.data.contact_number = "0000988383";
            req.data.UserId = req.data.email;

            var actual = svc.register(req);
            Assert.AreEqual(true, actual.Result.success);

        }
    }
}
