﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.api.Controllers;
using kanalitik.akka.actors.core;
using kanalitik.data.contract.apiRequest;
namespace kanalitik.api.Tests.Controllers
{
    [TestClass]
    public class MailControllerTest
    {
        public MailControllerTest()
        {
            BootstrapActor.Initialize();
        }
        [TestMethod]
        public void SendMailTest()
        {


            MailController Ctrl = new MailController();
            var actual = Ctrl.MailSave(new MailSaveRequestApi
            {
                configCode = "registration-notify",
                CultureName = "en-US",
                email = "irwan.ayeah@gmail.com",
                TokenUser = "",
                UserId = "irwan.ayeah@gmail.com"
            }).GetAwaiter().GetResult();

            Assert.AreEqual("OK", actual.Result.message);
            Assert.AreEqual(true, actual.Result.success);
            Assert.AreEqual(true, actual.Result.status);

        }


    }
}
