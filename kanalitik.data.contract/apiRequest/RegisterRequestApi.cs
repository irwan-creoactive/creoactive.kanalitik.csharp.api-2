﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class RegisterRequestApi:BaseRequest
    {
        public string name { set; get; }
        public string email { set; get; }
        public string business_type { set; get; }
        public string business_type_name { set; get; }
        public string contact_number { set; get; }
    }
}
