﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class PackageCompareRequest : BaseRequest
    {
        public string package_id { set; get; }
        public string client_package_id { set; get; }
    }
}
