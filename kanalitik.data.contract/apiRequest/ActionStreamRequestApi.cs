﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class ActionStreamRequestApi : BaseRequest
    {
        /// <summary>
        /// Nama action (like, unlike, etc)
        /// </summary>
        public string ActionName { set; get; }

        /// <summary>
        /// Kode Post yang diberikan action
        /// </summary>
        public string PostId { set; get; }

        /// <summary>
        /// terisi jika ternyata reply comment atau sejenisnya
        /// </summary>
        public string Message { set; get; }
    }
}
