﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class PackageActivationRequest : BaseRequest
    {
        public string package { set; get; }
    }
}
