﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class MailRequest : BaseRequest
    {
        public string email { set; get; }

        public string configCode { get; set; }
    }
}
