﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace kanalitik.data.contract.apiResponse
{
    public class PackageListResponse : BaseResponse
    {
        public string name { set; get; }
        public string price { set; get; }
        public List<PackageDetailResponse> social_media { set; get; }
        public List<PackageDetailResponse> blog { set; get; }
        public List<PackageDetailResponse> news_portal { set; get; }
    }



    public class PackageDetailResponse
    {
        public string package_detail_description { set; get; }
        /// <summary>
        /// berisi poin package detail description
        /// </summary>
    }
}
