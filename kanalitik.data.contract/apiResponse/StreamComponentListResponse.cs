﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class StreamComponentListResponse : BaseResponse
    {
        public List<StreamComponentResponse> StreamComponents { set; get; }
    }
}
