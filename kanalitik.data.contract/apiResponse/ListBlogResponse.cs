﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class ListBlogResponse : BaseResponse
    {
        public List<BlogResponse> Blogs { set; get; }
    }
}