﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class PackageOrderModel : BaseModel<PackageOrder>, IBaseModel<PackageOrder>
    {

        public PackageOrderModel() : base("PackageOrderModel") { }

        public async Task<PackageOrder> createOne(PackageOrder data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CreateRuleSet");
            Task<PackageOrder> result = null;
            if (validation.IsValid)
            {
                await this.db.PackageOrder.InsertOneAsync(data);
                result = Task<PackageOrder>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<PackageOrder> updateOne(PackageOrder data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UpdateRuleSet");

            Task<PackageOrder> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<PackageOrder>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.PackageOrder.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<PackageOrder>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<PackageOrder> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.PackageOrder.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<PackageOrder> data)
        {
            throw new NotImplementedException();
        }

        public async Task<PackageOrder> getById(string id)
        {
            var builderFilter = Builders<PackageOrder>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<PackageOrder> result = null;
            using (var cursor = await this.db.PackageOrder.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<PackageOrder>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<PackageOrder>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.PackageOrder.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<PackageOrder[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<PackageOrder[]> result = null;
            List<PackageOrder> res = new List<PackageOrder>();
            using (var cursor = await this.db.PackageOrder.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<PackageOrder[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
