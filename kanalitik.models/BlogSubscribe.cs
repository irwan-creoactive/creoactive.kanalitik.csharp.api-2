﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class BlogSubscribeModel : BaseModel<BlogSubscribe>, IBaseModel<BlogSubscribe>
    {

        public BlogSubscribeModel() : base("BlogSubscribeModel") { }

        public async Task<BlogSubscribe> createOne(BlogSubscribe data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "BlogSubscribeInsertRuleSet");
            Task<BlogSubscribe> result = null;
            if (validation.IsValid)
            {
                await this.db.BlogSubscribe.InsertOneAsync(data);
                result = Task<BlogSubscribe>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<BlogSubscribe> updateOne(BlogSubscribe data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "BlogSubscribeUpdateRuleSet");

            Task<BlogSubscribe> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<BlogSubscribe>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.BlogSubscribe.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<BlogSubscribe>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<BlogSubscribe> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.BlogSubscribe.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<BlogSubscribe> data)
        {
            throw new NotImplementedException();
        }

        public async Task<BlogSubscribe> getById(string id)
        {
            var builderFilter = Builders<BlogSubscribe>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<BlogSubscribe> result = null;
            using (var cursor = await this.db.BlogSubscribe.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<BlogSubscribe>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<BlogSubscribe>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.BlogSubscribe.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<BlogSubscribe[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<BlogSubscribe[]> result = null;
            List<BlogSubscribe> res = new List<BlogSubscribe>();
            using (var cursor = await this.db.BlogSubscribe.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<BlogSubscribe[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
