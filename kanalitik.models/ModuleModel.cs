﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class ModuleModel : BaseModel<Module>, IBaseModel<Module>
    {

        public ModuleModel() : base("ModuleModel") { }

        public async Task<Module> createOne(Module data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ModuleInsertRuleSet");
            Task<Module> result = null;
            if (validation.IsValid)
            {
                await this.db.Module.InsertOneAsync(data);
                result = Task<Module>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Module> updateOne(Module data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ModuleUpdateRuleSet");

            Task<Module> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Module>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Module.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Module>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Module> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "ModuleInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Module.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Module> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Module> getById(string id)
        {
            var builderFilter = Builders<Module>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Module> result = null;
            using (var cursor = await this.db.Module.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Module>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Module>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Module.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Module[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Module[]> result = null;
            List<Module> res = new List<Module>();
            using (var cursor = await this.db.Module.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Module[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
