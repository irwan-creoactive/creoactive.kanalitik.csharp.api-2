﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class ClientModel : BaseModel<Client>, IBaseModel<Client>
    {

        public ClientModel() : base("ClientModel") { }

        public async Task<Client> createOne(Client data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ClientRegisterRuleSet");
            Task<Client> result = null;
            if (validation.IsValid)
            {
                await this.db.Clients.InsertOneAsync(data);
                result = Task<Client>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Client> updateOne(Client data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ClientUpdateRuleSet");

            Task<Client> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Client>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Clients.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Client>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Client> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "ClientRegisterRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Clients.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Client> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Client> getById(string id)
        {
            var builderFilter = Builders<Client>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Client> result = null;
            using (var cursor = await this.db.Clients.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Client>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Client>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Clients.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Client[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Client[]> result = null;
            List<Client> res = new List<Client>();
            using (var cursor = await this.db.Clients.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Client[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
