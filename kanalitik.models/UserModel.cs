﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{
    
    
    public class UserModel : BaseModel<User>, IBaseModel<User>
    {
        
        public UserModel(): base("UserModel") {}

        public async Task<User> createOne(User data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UserRegisterRuleSet");
            Task<User> result = null;
            if (validation.IsValid)
            {
               await this.db.Users.InsertOneAsync(data);
               result = Task<User>.Run(() => {
                   return data;
               });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
             return await result;
        }

        public async Task<User> updateOne(User data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UserUpdateRuleSet");
            
            Task<User> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<User>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Users.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<User>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<User> data)
        {
             // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data) { 
                 var validation = this.validateModel(item, "UserRegisterRuleSet");
                 if (!validation.IsValid)
                 {
                     this.throwErrorSystem(validation);
                     break;
                 }
                 success = 1;
            }
            if (success==1)
            {
               await this.db.Users.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<User> data)
        {
            throw new NotImplementedException();
        }

        public async Task<User> getById(string id)
        {
            var builderFilter = Builders<User>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<User> result = null;
            using (var cursor = await this.db.Users.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null) {
                            result = Task<User>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
               var builderFilter = Builders<User>.Filter.Eq(a=>a.id,new ObjectId(id));
               await this.db.Users.FindOneAndDeleteAsync(builderFilter);
               Task<string> result = Task<string>.Run(() =>
               {
                   return id;
               });
               return await result;
        }
        
        public async Task<User[]> list(BsonDocument filter,BsonDocument sort, int skip, int limit)
        {

            Task<User[]> result = null;
             List<User> res = new List<User>();
            using (var cursor = await this.db.Users.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }
            
                result = Task<User[]>.Run(() =>
                {
                    return res.ToArray();
                });
            

            return await result;
        }

        /// <summary>
        /// ini adalah contoh extend method 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<User> isEmailExist(string email)
        {
            var builderFilter = Builders<User>.Filter;
            var filter = builderFilter.Eq(a => a.email, email);

            Task<User> result = null;

            int i = 0;
            using (var cursor = await this.db.Users.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            i++;
                            result = Task<User>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }

                if (i == 0)
                {
                    result = Task<User>.Run(() =>
                    {
                        return new User();
                    });
                }
            }
            return await result;
        }

    }
}
