﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class FeatureModel : BaseModel<Feature>, IBaseModel<Feature>
    {

        public FeatureModel() : base("FeatureModel") { }

        public async Task<Feature> createOne(Feature data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "FeatureInsertRuleSet");
            Task<Feature> result = null;
            if (validation.IsValid)
            {
                await this.db.Feature.InsertOneAsync(data);
                result = Task<Feature>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Feature> updateOne(Feature data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "FeatureUpdateRuleSet");

            Task<Feature> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Feature>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Feature.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Feature>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Feature> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "FeatureInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Feature.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Feature> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Feature> getById(string id)
        {
            var builderFilter = Builders<Feature>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Feature> result = null;
            using (var cursor = await this.db.Feature.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Feature>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Feature>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Feature.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Feature[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Feature[]> result = null;
            List<Feature> res = new List<Feature>();
            using (var cursor = await this.db.Feature.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Feature[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
