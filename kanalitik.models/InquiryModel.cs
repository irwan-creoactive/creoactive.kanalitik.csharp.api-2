﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{
    public class InquiryModel : BaseModel<Inquiry>, IBaseModel<Inquiry>
    {
        public InquiryModel() : base("InquiryModel") { }

        public async Task<Inquiry> createOne(Inquiry data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "InquiryCreateRuleSet");
            Task<Inquiry> result = null;
            if (validation.IsValid)
            {
                await this.db.Inquiry.InsertOneAsync(data);
                result = Task<Inquiry>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Inquiry> updateOne(Inquiry data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "InquiryUpdateRuleSet");

            Task<Inquiry> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Inquiry>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Inquiry.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Inquiry>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Inquiry> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "InquiryCreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Inquiry.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Inquiry> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Inquiry> getById(string id)
        {
            var builderFilter = Builders<Inquiry>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Inquiry> result = null;
            using (var cursor = await this.db.Inquiry.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Inquiry>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Inquiry>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Inquiry.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Inquiry[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Inquiry[]> result = null;
            List<Inquiry> res = new List<Inquiry>();
            using (var cursor = await this.db.Inquiry.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Inquiry[]>.Run(() =>
                {
                    return res.ToArray();
                });

            return await result;
        }
    }
}
