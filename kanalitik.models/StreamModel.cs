﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class StreamModel : BaseModel<Stream>, IBaseModel<Stream>
    {

        public StreamModel() : base("StreamModel") { }

        public async Task<Stream> createOne(Stream data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CreateRuleSet");
            Task<Stream> result = null;
            if (validation.IsValid)
            {
                await this.db.Stream.InsertOneAsync(data);
                result = Task<Stream>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Stream> updateOne(Stream data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UpdateRuleSet");

            Task<Stream> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Stream>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Stream.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Stream>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Stream> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Stream.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Stream> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Stream> getById(string id)
        {
            var builderFilter = Builders<Stream>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Stream> result = null;
            using (var cursor = await this.db.Stream.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Stream>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Stream>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Stream.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Stream[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Stream[]> result = null;
            List<Stream> res = new List<Stream>();
            using (var cursor = await this.db.Stream.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Stream[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
