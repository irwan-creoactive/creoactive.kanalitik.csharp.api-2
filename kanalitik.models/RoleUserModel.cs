﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class RoleUserModel : BaseModel<RoleUser>, IBaseModel<RoleUser>
    {

        public RoleUserModel() : base("RoleUserModel") { }

        public async Task<RoleUser> createOne(RoleUser data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "RoleUserInsertRuleSet");
            Task<RoleUser> result = null;
            if (validation.IsValid)
            {
                await this.db.RoleUser.InsertOneAsync(data);
                result = Task<RoleUser>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<RoleUser> updateOne(RoleUser data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "RoleUserUpdateRuleSet");

            Task<RoleUser> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<RoleUser>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.RoleUser.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<RoleUser>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<RoleUser> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "RoleUserInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.RoleUser.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<RoleUser> data)
        {
            throw new NotImplementedException();
        }

        public async Task<RoleUser> getById(string id)
        {
            var builderFilter = Builders<RoleUser>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<RoleUser> result = null;
            using (var cursor = await this.db.RoleUser.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<RoleUser>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<RoleUser>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.RoleUser.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<RoleUser[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<RoleUser[]> result = null;
            List<RoleUser> res = new List<RoleUser>();
            using (var cursor = await this.db.RoleUser.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<RoleUser[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
