﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.models
{
    public interface IBaseModel<T>
    {
        Task<T> createOne(T data);
        Task<T> updateOne(T data);
        Task createMany(List<T> data);
        Task updateMany(List<T> data);// TODO
        Task<T> getById(string id);
        Task<string> deleteById(string id);
        Task<T[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit);
    }
}
