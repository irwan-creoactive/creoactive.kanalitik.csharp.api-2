﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.models
{
    public class UpdateBuilderModel<T>
    {
        protected BsonArray isCollection(object value)
        {
            BsonArray arr = new BsonArray { };
            foreach (var item in (IEnumerable)value)
            {
                var anu = item.ToBsonDocument();
                var bval = anu.Where(a => a.Name != "_t");
                BsonDocument subDoc = new BsonDocument();
                foreach (var bitem in bval)
                {
                    subDoc.Add(bitem);
                }
                arr.Add(subDoc);
            }
            return arr;
        }

        public string fieldSet(T data)
        {
            var builder = new BsonDocument();

            Type t = typeof(T);
            var fields = t.GetProperties();

            try
            {
                foreach (var field in fields)
                {
                    string name = field.Name;
                    string PropertyType = field.PropertyType.Name;
                    if (name != "id")
                    {
                        var value = field.GetValue(data);
                        if (PropertyType.CompareTo("DateTime") == 0 && ((DateTime)value).Year == 1)
                        {
                            continue;
                        }

                        if (value != null)
                        {
                            // TODO
                            if (value.GetType().IsGenericType && value is IEnumerable) // Jika kondisi List
                            {
                                builder.Add(new BsonElement(name, this.isCollection(value)));
                            }
                            else
                            {
                                builder.Add(new BsonElement(name, BsonValue.Create(value)));
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new Exception("Fatal Error Update Builder. Please Contact Developer");
            }

            return new BsonDocument(new BsonElement("$set", builder)).ToJson();
        }
    }
}
