﻿using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.repositories.extends;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.interfaces
{
    public interface IMasterDataService
    {
        ReaderConfigFromDB ConfiDb { get;  }

        #region User
        ActorResponse<UserResponse> saveUser(ActorRequest<UserRequest> request);
        ActorResponse<UserResponse> readUser(ActorRequest<UserRequest> request);
        #endregion

        #region User Packages
        ActorResponse<UserPackagesResponse> saveUserPackages(ActorRequest<UserPackagesRequest> request);
        ActorResponse<UserPackagesResponse> readUserPackages(ActorRequest<UserPackagesRequest> request);
        #endregion

        #region Master Data Package
        ActorResponse<MasterDataPackageResponse> saveMasterDataPackage(ActorRequest<MasterDataPackageRequest> request);
        ActorResponse<MasterDataPackageResponse> readMasterDataPackage(ActorRequest<MasterDataPackageRequest> request);

        #region Master Data Packages Detail
        ActorResponse<MasterDataPackageDetailResponse> saveMasterDataDetailPackage(ActorRequest<MasterDataPackageDetailRequest> request);
        ActorResponse<MasterDataPackageDetailResponse> readMasterDataDetailPackage(ActorRequest<MasterDataPackageDetailRequest> request);
        #endregion
        #endregion

        #region Invoices
        ActorResponse<InvoicesResponse> saveInvoices(ActorRequest<InvoicesRequest> request);
        ActorResponse<InvoicesResponse> readInvoices(ActorRequest<InvoicesRequest> request);
        #endregion

        #region Role Menu
        ActorResponse<RoleMenuResponse> saveInvoices(ActorRequest<RoleMenuRequest> request);
        ActorResponse<RoleMenuResponse> readInvoices(ActorRequest<RoleMenuRequest> request);
        #endregion

        #region Master Data Menu Package
        ActorResponse<MasterDataMenuPackageResponse> saveInvoices(ActorRequest<MasterDataMenuPackageRequest> request);
        ActorResponse<MasterDataMenuPackageResponse> readInvoices(ActorRequest<MasterDataMenuPackageRequest> request);
        #endregion

        #region Blog
        ActorResponse<BlogResponse> saveInvoices(ActorRequest<BlogRequestApi> request);
        ActorResponse<BlogResponse> readInvoices(ActorRequest<BlogRequestApi> request);
        #endregion

        #region Blog Comment
        ActorResponse<BlogCommentResponse> saveInvoices(ActorRequest<BlogCommentRequestApi> request);
        ActorResponse<BlogCommentResponse> readInvoices(ActorRequest<BlogCommentRequestApi> request);
        #endregion

        #region Feedback
        ActorResponse<FeedbackResponse> saveInvoices(ActorRequest<FeedbackRequest> request);
        ActorResponse<FeedbackResponse> readInvoices(ActorRequest<FeedbackRequest> request);
        #endregion

        #region Request Demo
        ActorResponse<RequestDemoResponse> saveInvoices(ActorRequest<RequestDemoRequest> request);
        ActorResponse<RequestDemoResponse> readInvoices(ActorRequest<RequestDemoRequest> request);
        #endregion
    }
}
