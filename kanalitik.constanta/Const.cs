﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.constanta
{
    public class Const
    {
        public static string PathBaseDir = AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug", "");
        public static string actorUserPath = "/user/{0}";
        public static TimeSpan DefaultTimeout = TimeSpan.FromSeconds(15);
        public static TimeSpan TickActorScheduler = TimeSpan.FromSeconds(5);
        public static TimeSpan DelayActorScheduler = TimeSpan.FromSeconds(1);
    }

    public enum BusinessTypeEnum { Personal = 0, Company = 1, Organization = 2 }
    public enum RoleUserEnum
    {
        Account_Owner_Demo = 0,
        Account_Owner_Free = 1,
        Account_Owner = 2,
        Account_Administrator = 3,
        Client_Service = 4,
        Admin_Head = 5,
        Basic_Admin = 6,
        System_Administrator = 7,
        Finance_Staff = 8
    }
}
